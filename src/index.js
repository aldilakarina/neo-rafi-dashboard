import React from "react";
import ReactDOM from "react-dom";
import { Router } from "react-router-dom";
import { Provider } from 'react-redux'
import { history, store } from 'redux/store'
import App from './App';
import "assets/css/material-dashboard-react.css?v=1.3.0";
import moment from "moment";
import 'moment/locale/id';

moment.locale();

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <App />
    </Router>
  </Provider>,
  document.getElementById("root")
);
