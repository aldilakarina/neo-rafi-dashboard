import { ACTIONS } from 'constants/index';
import fetch from 'utils/fetch';
import { SERVICES } from 'configs';

export function fetchTimelineList(data) {
  return dispatch => {
    const options = {
      method: 'get',
      url: SERVICES.GET_TIMELINE_REVIEW + '?page=' + data.page + '&size=' + data.count,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(listOTimelineFetchedAction(res, data.page, data.count));
        dispatch(doneLoadingAction());
      })
      .catch(() => {
        dispatch(listOTimelineFetchedAction([], data.page, data.count));
        dispatch(doneLoadingAction());
      });
  };
}

export function fetchTimelineEdit(data) {
  return dispatch => {
    const options = {
      method: 'post',
      url: SERVICES.POST_TIMELINE_APPROVAL,
      data,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(editOfTimelineFetchedAction(res));
        dispatch(doneLoadingAction());
        window.location.href = '/dashboard';
      })
      .catch(() => {
        dispatch(editOfTimelineFetchedAction([]));
        dispatch(doneLoadingAction());
      });
  };
}

function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function listOTimelineFetchedAction(data, page, count) {
  return {
    type: ACTIONS.LIST_OF_TIMELINE_FETCHED,
    data,
    page,
    count
  };
}

function editOfTimelineFetchedAction(data) {
  return {
    type: ACTIONS.APPROVE_TIMELINE,
    data
  };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}