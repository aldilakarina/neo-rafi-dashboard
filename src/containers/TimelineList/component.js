import React from 'react';
import PropTypes from "prop-types";
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import Edit from "@material-ui/icons/Edit";
import { ACTIONS } from 'constants/index';
import Modal from 'components/Modal/Modal.jsx';
import UltimatePaging from 'components/forms/UltimatePaging';

export default class Component extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      render: false,
      open: false,
      item: null,
      value: 0,
      page: 1,
      total: 3,
    };
    this._closeModal = this._closeModal.bind(this);
    this._handleApproveTimeline = this._handleApproveTimeline.bind(this);
    this._onPageChange = this._onPageChange.bind(this);
  }

  componentWillMount() {
    let { actions, page } = this.props;

    actions.fetchTimelineList({ page: (page === undefined ? 1 : page), count: 21 });
  }

  componentWillReceiveProps(nextProps) {
    let { LIST_OF_TIMELINE_FETCHED } = ACTIONS;
    let { type } = nextProps;
    if (type === LIST_OF_TIMELINE_FETCHED) {
      this.setState({render:true});
    }
  }

  _handleChange = (event, value) => {
    let { actions, page } = this.props;

    actions.fetchTimelineList({ page: (page === undefined ? 1 : page), count: 21 });
  };

  _handleApproveTimeline = (value) => {
    const { actions } = this.props;
    actions.fetchTimelineEdit({postId:this.state.item.id, approveStatus: value});
    this._closeModal();
  }

  _onPageChange(page) {
    let { actions } = this.props;
    let { meta } = this.props.data;

    this.setState({ page });
    actions.fetchTimelineList({ page, count: meta.size });
  }

  _openModal(item) {
    this.setState({ open: true, item });
  }

  _closeModal() {
    this.setState({ open: false, item:null });
  }

  _renderModal() {
    let { classes } = this.props;
    return (
      <Modal open={this.state.open} closeModal={this._closeModal} onClose={() => this._handleApproveTimeline(false)} onClick={() => this._handleApproveTimeline(true)}>
        {this.state.item !== null ?
          <div>
            <img src={this.state.item.photo} alt='aww' width="100%" />
            <h6 className={classes.modalTitle}>
              {
                'Approve: ' + this.state.item.title
              }
            </h6>
            <p>
              {Array.isArray(this.state.item.category) ? this.state.item.category.join(', ') : this.state.item.category}
            </p>
        </div>
          :
          <div>
          no data
          </div>
        }
      </Modal>
    );
  }

  _renderMeta() {
    let { classes, data, page } = this.props;

    return (
      <div className={classes.pagging}>
        {!this.state.render ? 
          <h6>loading...</h6> 
        : 
          <UltimatePaging
            totalPages={(data.meta.totalPage !== undefined) 
              ? data.meta.totalPage : 1}
            currentPage={page}
            onChange={this._onPageChange}
          />
        }
      </div>
    );
  }

  render() {
    let { classes, data } = this.props;

    return (
      <section>
        {!this.state.render ? 
            <h6>loading...</h6> 
          : 
            <GridList cellHeight={160} className={classes.gridList} cols={3}>
              {data.data.map((tile, index) => (
                <GridListTile key={index} cols={tile.cols || 1}>
                  <img src={tile.photo} alt={tile.title} />
                  <GridListTileBar
                    title={tile.title}
                    subtitle={<span>Caption: {tile.caption}</span>}
                    actionIcon={
                      <IconButton
                        onClick={() => this._openModal(tile)}
                        aria-label="Close"
                        className={classes.tableActionButton}
                      >
                        <Edit
                          className={
                            classes.tableActionButtonIcon + " " + classes.close
                          }
                        />
                      </IconButton>
                    }
                  />
                </GridListTile>
              ))}
            </GridList>
        }
        {this._renderMeta()}
        {this._renderModal()}
      </section>
    );
  }

}

Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
  data: PropTypes.array,
  isLoading: PropTypes.bool,
  page: PropTypes.number,
  count: PropTypes.number
};