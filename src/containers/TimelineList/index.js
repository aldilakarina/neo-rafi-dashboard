import withStyles from "@material-ui/core/styles/withStyles";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './action';
import Component from './component';
import styles from './styles';

function mapStateToProps(state) {
  const { data, page, count, type } = state.timelineList;
  const { isLoading } = state.loading;
  
  return {
    isLoading,
    data,
    page,
    count,
    type
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

const Styled = withStyles(styles)(Component);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Styled);
