import React from 'react';
import PropTypes from "prop-types";
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import { ACTIONS } from 'constants/index';
import UltimatePaging from 'components/forms/UltimatePaging';

export default class Component extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      render: false,
      open: false,
      item: null,
      value: 0,
      page: 1,
      total: 3,
    };
    this._onPageChange = this._onPageChange.bind(this);
  }

  componentWillMount() {
    let { actions, page } = this.props;

    actions.fetchRejectedList({ page: (page === undefined ? 1 : page), count: 21 });
  }

  componentWillReceiveProps(nextProps) {
    let { LIST_OF_TIMELINE_REJECTED_FETCHED } = ACTIONS;
    let { type } = nextProps;
    if (type === LIST_OF_TIMELINE_REJECTED_FETCHED) {
      this.setState({render:true});
    }
  }

  _onPageChange(page) {
    let { actions } = this.props;
    let { meta } = this.props.data;

    this.setState({ page });
    actions.fetchTimelineList({ page, count: meta.size });
  }

  _handleChange = (event, value) => {
    let { actions, page } = this.props;

    actions.fetchRejectedList({ page: (page === undefined ? 1 : page), count: 21 });
  };

  _renderMeta() {
    let { classes, data, page } = this.props;

    return (
      <div className={classes.pagging}>
        {!this.state.render ? 
          <h6>loading...</h6> 
        : 
          <UltimatePaging
            totalPages={(data.meta.totalPage !== undefined) 
              ? data.meta.totalPage : 1}
            currentPage={page}
            onChange={this._onPageChange}
          />
        }
      </div>
    );
  }

  render() {
    let { classes, data } = this.props;

    return (
      <section>
        {!this.state.render || data.data === undefined ? 
            <h6>loading...</h6> 
          : 
            <GridList cellHeight={160} className={classes.gridList} cols={3}>
              {data.data.map((tile, index) => (
                <GridListTile key={index} cols={tile.cols || 1}>
                  <img src={tile.photo} alt={tile.title} />
                  <GridListTileBar
                    title={tile.title}
                    subtitle={<span>Caption: {tile.caption}</span>}
                  />
                </GridListTile>
              ))}
            </GridList>
        }
        {this._renderMeta()}
      </section>
    );
  }

}

Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
  data: PropTypes.array,
  isLoading: PropTypes.bool,
  page: PropTypes.number,
  count: PropTypes.number
};