import { ACTIONS } from 'constants/index';

const initialState = {
  data: []
};

export default function reducer(state = initialState, action) {
  const { LIST_OF_TIMELINE_REJECTED_FETCHED } = ACTIONS;
  const { type, data, page, count } = action;

  switch (type) {
    case LIST_OF_TIMELINE_REJECTED_FETCHED:
      return {
        ...state,
        isLoading: false,
        data,
        page,
        count,
        type
      };
    default:
      return state;
  }
}
