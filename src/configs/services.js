const BASE_URL = 'http://ec2-13-250-179-236.ap-southeast-1.compute.amazonaws.com:9000/api/v1';
const BASE_URL_OLD = 'http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000';
// const BASE_URL = 'http://localhost:9000';

const services = {
  GET_NEWS_LIST: `${BASE_URL_OLD}/news_event_top`,
  GET_NEWS_SECTION_LIST: `${BASE_URL_OLD}/news_event`,
  GET_NEWS_DETAIL: `${BASE_URL_OLD}/news_event_id`,
  ADD_NEWS: `${BASE_URL_OLD}/news_event`,
  DELETE_NEWS: `${BASE_URL_OLD}/news_event`,
  EDIT_NEWS: `${BASE_URL_OLD}/news_event`,

  GET_PROMO_LIST: `${BASE_URL_OLD}/listpromos`,
  GET_PROMO_DETAIL: `${BASE_URL_OLD}/listpromos`,
  ADD_PROMO: `/listpromos`,
  EDIT_PROMO: `/listpromos`,
  DELETE_PROMO: `${BASE_URL_OLD}/listpromos`,
  PROMO_URL: `${BASE_URL_OLD}`,

  GET_USER_LIST: `${BASE_URL}/users/`,
  DELETE_USER: `${BASE_URL}/users/`,
  GET_POST_LIST_BY_USER: `${BASE_URL}/users`,
  DELETE_POST: `${BASE_URL}/users`,

  GET_REWARD_LIST: `${BASE_URL}/point/reward`,
  // GET_REDEEM_REWARD_LIST: 
  //`${BASE_URL}/point/d12a6f03-52ac-466d-801b-b46da7f9b21d/redeemed-reward`,
  GET_REDEEM_REWARD_LIST: `${BASE_URL}/rewards`,
  EDIT_REDEEM: `${BASE_URL}/user/reward/coupon`,

  GET_FRAME_LIST: `${BASE_URL}/frame`,
  ADD_FRAME_LIST: `${BASE_URL}/frame`,
  EDIT_FRAME_BY_ID: `${BASE_URL}/update-frame/`,
  GET_FRAME_BY_ID: `${BASE_URL}/frame/`,
  DELETE_FRAME: `${BASE_URL}/delete-frame/`,

  LOGIN: `${BASE_URL}/login-user`,

  GET_TIMELINE_REVIEW: `${BASE_URL}/timeline/review`,
  GET_TIMELINE_REJECTED: `${BASE_URL}/timeline/rejected`,
  POST_TIMELINE_APPROVAL: `${BASE_URL}/timeline/review/approval`
};

export default services;