const routes = {
  HOME() { return `/`; },
  LOGIN() { return `/login`; },
  
  DASHBOARD() { return `/dashboard`; },
  LIST_NEWS() { return `/news`; },
  DETAIL_NEWS(id) { return `/newsdetail/${id}`; },
  CREATE_NEWS() { return `/newsadd`; },
  EDIT_NEWS(id) { return `/newsedit/${id}`; },
  
  LIST_PROMO() { return `/promo`; },
  DETAIL_PROMO(id) { return `/promodetail/${id}`; },
  CREATE_PROMO() { return `/promoadd`; },
  EDIT_PROMO(id) { return `/promoedit/${id}`; },
  
  LIST_USER() { return `/user`; },
  DETAIL_USER(id) { return `/userdetail/${id}`; },
  
  LIST_REWARD() { return `/reward`; },
  LIST_REDEEM() { return `/redeem`; },
  REWARD_BY_USER(id) { return `/user/${id}/reward`; },
  REVIEW_BY_USER(id) { return `/user/${id}/review`; },
  
  LIST_FRAME() { return `/frame`; },
  CREATE_FRAME() { return `/frameadd`; },
  EDIT_FRAME(id) { return `/frameedit/${id}`; },
  
  CONFIG_REWARD() { return `/reward`; },
  CONFIG_APP() { return `/config`; }
};

export default routes;