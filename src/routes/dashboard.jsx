// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import ContentPaste from "@material-ui/icons/ContentPaste";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
import Photo from "@material-ui/icons/Photo";

import views from 'views';

import { ROUTES } from 'configs'

let { TimelineReviewPage, NewsListPage, UserListPage, RewardListPage, RedeemListPage, FrameListPage, PromoListPage } = views;

const dashboardRoutes = [
  {
    path: ROUTES.DASHBOARD(),
    sidebarName: "Timelie",
    navbarName: "Timeline List",
    icon: Dashboard,
    component: TimelineReviewPage
  },
  {
    path: ROUTES.LIST_NEWS(),
    sidebarName: "News",
    navbarName: "News List",
    icon: Dashboard,
    component: NewsListPage
  },
  {
    path: ROUTES.LIST_USER(),
    sidebarName: "Users List",
    navbarName: "Users List",
    icon: Person,
    component: UserListPage
  },
  {
    path: ROUTES.LIST_REWARD(),
    sidebarName: "Reward List",
    navbarName: "Reward List",
    icon: ContentPaste,
    component: RewardListPage
  },
  {
    path: ROUTES.LIST_REDEEM(),
    sidebarName: "Redeem List",
    navbarName: "Redeem List",
    icon: LibraryBooks,
    component: RedeemListPage
  },
  {
    path: ROUTES.LIST_PROMO(),
    sidebarName: "Promo List",
    navbarName: "Promo List",
    icon: BubbleChart,
    component: PromoListPage
  },
  {
    path: ROUTES.LIST_FRAME(),
    sidebarName: "Frame List",
    navbarName: "Frame List",
    icon: Photo,
    component: FrameListPage
  },
  { redirect: true, path: "/", to: "/dashboard", navbarName: "Redirect" }
];

export default dashboardRoutes;
