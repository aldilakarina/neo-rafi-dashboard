import React from "react";
import CustomInput from "components/CustomInput/CustomInput.jsx";

const TextField = ({
  input,
  ...custom
}) => (
  <CustomInput
    {...input}
    {...custom}
  />
)
export default TextField;
