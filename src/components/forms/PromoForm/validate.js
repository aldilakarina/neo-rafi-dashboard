import validateInput from 'utils/validateInput';

const validate = values => {
  const {
    listNewsPromoName,
    listNewsPromoDescription,
    listNewsPromoImage,
    listNewsPromoUrl,
    listNewsCategory,
    liststartDate,
    listoutDate,
  } = values;
  const errors = {
    listNewsPromoName: validateInput(listNewsPromoName, ['required', 'max-255']),
    listNewsPromoDescription: validateInput(listNewsPromoDescription, ['required']),
    listNewsPromoImage: validateInput(listNewsPromoImage, ['required']),
    listNewsPromoUrl: validateInput(listNewsPromoUrl, ['required', 'max-255']),
    listNewsCategory: validateInput(listNewsCategory, ['required']),
    liststartDate: validateInput(liststartDate, ['required']),
    listoutDate: validateInput(listoutDate, ['required']),
  };
  
  return errors;
};
 

export default validate;