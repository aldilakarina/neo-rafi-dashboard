import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import withStyles from "@material-ui/core/styles/withStyles";
import Component from './component';
import styles from './styles';
import validate from './validate';
import moment from "moment";

function mapStateToProps(state) {
  const { createPromo } = state.form;
  let values = {};

  if (createPromo && createPromo.values) values = createPromo.values;

  return {
    values
  };
}

function mapDispatchToProps() {
  return {
  };
}

const Styled = withStyles(styles)(Component);

const Connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(Styled);

export default reduxForm({
  form: 'promoForm',
  validate,
  initialValues: {
    listNewsPromoImage: '',
    liststartDate: moment().format('YYYY-MM-DD'),
    listoutDate: moment().format('YYYY-MM-DD'),
  },
  enableReinitialize: true
})(Connected);