import React from 'react';
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import FrameForm from "components/forms/FrameForm";

export default class Component extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      render: false,
      image: null
    };
  }

  _handleCreateFrame = (data) => {
    data = Object.assign(data, {frameImage:this.state.image})
    let { actions } = this.props;

    actions.fetchFrameCreate(data);
  }

  _handleUpload = (image) => {
    this.setState({image});
  }

  render() {
    return (
      <Grid container>
        <GridItem xs={12} sm={12} md={8}>
          <Card>
            <FrameForm label="Create New Frame" onSubmit={(data) => { this._handleCreateFrame(data) }} onUpload={(file) => this._handleUpload(file)} />
          </Card>
        </GridItem>
      </Grid>
    );
  }

}

Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
};