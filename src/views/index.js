import LoginPage from './Login';
import CreateNewsPage from './CreateNews';
import EditNewsPage from './EditNews';
import NewsListPage from './NewsList';
import NewsDetailPage from './NewsDetail';
import UserListPage from './UserList';
import RewardListPage from './RewardList';
import RedeemListPage from './RedeemList';
import CreateFramePage from './CreateFrame';
import EditFramePage from './EditFrame';
import FrameListPage from './FrameList';
import CreatePromoPage from './CreatePromo';
import EditPromoPage from './EditPromo';
import PromoListPage from './PromoList';
import PromoDetailPage from './PromoDetail';
import UserDetailPage from './UserDetail';
import TimelineReviewPage from './TimelineReview';

const views = {
  LoginPage,
  CreateNewsPage,
  EditNewsPage,
  NewsListPage,
  NewsDetailPage,
  UserListPage,
  RewardListPage,
  RedeemListPage,
  CreateFramePage,
  EditFramePage,
  FrameListPage,
  CreatePromoPage,
  EditPromoPage,
  PromoListPage,
  PromoDetailPage,
  UserDetailPage,
  TimelineReviewPage,
};

export default views;