import { ACTIONS } from 'constants/index';
import fetch from 'utils/fetch';
import { SERVICES } from 'configs';

export function fetchPromoEdit(data) {
  let {
    _id,
    ...value
  } = data;
  return dispatch => {
    const options = {
      method: 'put',
      url: `${SERVICES.PROMO_URL}/${value.promo_id}/${SERVICES.EDIT_PROMO}/${_id}`,
      value,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(editOfPromoFetchedAction(res));
        dispatch(doneLoadingAction());
        window.location.href = '/promo';
      })
      .catch(() => {
        dispatch(editOfPromoFetchedAction([]));
        dispatch(doneLoadingAction());
      });
  };
}


export function fenchGetDetailPromo(id) {
  return dispatch => {
    const options = {
      method: 'get',
      url: `${SERVICES.GET_PROMO_DETAIL}/${id}`,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(rest => {
        dispatch(detailOfPromoFetchedAction(rest));
        dispatch(doneLoadingAction());
      })
      .catch(() => {
        dispatch(detailOfPromoFetchedAction({}));
        dispatch(doneLoadingAction());
      });
  };
}

function detailOfPromoFetchedAction(data) {
  return {
    type: ACTIONS.DETAIL_OF_PROMO_FETCHED,
    data
  };
}

function editOfPromoFetchedAction(data) {
  return {
    type: ACTIONS.EDIT_PROMO,
    data
  };
}

function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}