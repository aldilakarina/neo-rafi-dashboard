import { ACTIONS } from 'constants/index';

const initialState = {
  data: {}
};

export default function reducer(state = initialState, action) {
  const { GET_OF_FRAME_BY_ID_FETCHED } = ACTIONS;
  const { type, data } = action;

  switch (type) {
    case GET_OF_FRAME_BY_ID_FETCHED:
      return {
        ...state,
        isLoading: false,
        data,
        type
      };
    default:
      return state;
  }
}
