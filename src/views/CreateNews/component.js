import React from 'react';
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import NewsForm from "components/forms/NewsForm";
import moment from "moment";

export default class Component extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      render: false,
      image: null
    };
  }

  _handleCreateNews = (data) => {
    data = Object.assign(data, {
      newsImage: this.state.image,
      newsStartDate: moment().format('YYYY-MM-DDT00:00:00.000Z'),
      newsEndDate: moment().format('YYYY-MM-DDT00:00:00.000Z'),
      newsDisasterImpact:"",
      phoneNumber: ""
    });
    let { actions } = this.props;

//    console.log(data)
    actions.fetchNewsCreate(data);
  }

  _handleUpload = (image) => {
    this.setState({image});
  }

  render() {
    return (
      <Grid container>
        <GridItem xs={12} sm={12} md={8}>
          <Card>
            <NewsForm label={'Create New News'} onSubmit={(data) => { this._handleCreateNews(data) }} onUpload={(file) => this._handleUpload(file)} />
          </Card>
        </GridItem>
      </Grid>
    );
  }

}

Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
};