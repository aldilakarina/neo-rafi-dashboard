import React from "react";
import PropTypes from "prop-types";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import LoginForm from 'components/forms/Login';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cardAnimaton: "cardHidden"
    };
    this._handleSubmit = this._handleSubmit.bind(this);
  }

  componentDidMount() {
    setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }

  _handleSubmit(values) {
    let { actions } = this.props;

    actions.fetchLogin(values);
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <div className={classes.pageHeader}>
          <div className={classes.container}>
            <GridContainer justify="center">
              <GridItem xs={12} sm={12} md={4}>
                <Card className={classes[this.state.cardAnimaton]}>
                  <LoginForm onSubmit={this._handleSubmit} />
                </Card>
              </GridItem>
            </GridContainer>
          </div>
        </div>
      </div>
    );
  }
}
Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
};