import React from 'react';
import PropTypes from "prop-types";
import TravelNews from 'containers/TravelNews';
import CulinaryNews from 'containers/CulinaryNews';
import AsianGames from 'containers/AsianGames';
import CustomTabs from "components/CustomTabs/CustomTabs.jsx";
import NoteAdd from "@material-ui/icons/NoteAdd";
import IconButton from '@material-ui/core/IconButton';
import { Link } from 'react-router-dom';
import { ROUTES } from 'configs';

export default class Component extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      id: null,
      title: '',
    };
    this._closeModal = this._closeModal.bind(this);
    this._handleDeleteNews = this._handleDeleteNews.bind(this);
  }

  _handleDeleteNews = () => {
    const { actions } = this.props;
    actions.fetchNewsDelete(this.state.id);
    this.setState({ open: false, id:null, title:'' });
  }

  _openModal(id, title) {
    this.setState({ open: true, id, title });
  }

  _closeModal() {
    this.setState({ open: false, id:null, title:'' });
  }

  render() {
    let { classes } = this.props;

    return (
      <CustomTabs
        plainTabs={true}
        headerColor="primary"
        title = {
          <Link to={ROUTES.CREATE_NEWS()}>
            <IconButton
              aria-label="Close"
              className={classes.tableActionButton}
            >
              <NoteAdd />
            </IconButton>
          </Link>
        }
        tabs={[
          {
            tabName: "Wisata",
            tabContent: (
              <TravelNews />
            )
          },
          {
            tabName: "Kuliner",
            tabContent: (
              <CulinaryNews />
            )
          },
          {
            tabName: "Asian Games",
            tabContent: (
              <AsianGames />
            )
          },
        ]}
      />
    );
  }

}

Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
};