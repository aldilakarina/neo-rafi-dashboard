import withStyles from "@material-ui/core/styles/withStyles";
import Component from './component';
import styles from './styles';

export default withStyles(styles)(Component);
