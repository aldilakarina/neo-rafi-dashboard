import { ACTIONS } from 'constants/index';

const initialState = {
  data:{}
};

export default function reducer(state = initialState, action) {
  const { LIST_OF_POST_BY_USER_FETCHED } = ACTIONS;
  const { type, data } = action;

  switch (type) {
    case LIST_OF_POST_BY_USER_FETCHED:
      return {
        ...state,
        isLoading: false,
        data,
        type
      };
    default:
      return state;
  }
}