import {
  primaryColor,
  dangerColor,
} from "assets/jss/material-dashboard-react.jsx";
import modalStyle from "assets/jss/material-dashboard-react/modalStyle.jsx";
import typographyStyle from "assets/jss/material-dashboard-react/components/typographyStyle.jsx";

const styles = {
  ...typographyStyle,
  ...modalStyle,
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  },
  tableActionButton: {
    width: "27px",
    height: "27px"
  },
  tableActionButtonIcon: {
    width: "17px",
    height: "17px"
  },
  edit: {
    backgroundColor: "transparent",
    color: primaryColor,
    boxShadow: "none"
  },
  close: {
    backgroundColor: "transparent",
    color: dangerColor,
    boxShadow: "none"
  },
  modalBody: {
    paddingTop: "24px",
    paddingRight: "0px",
    paddingBottom: "16px",
    paddingLeft: "0px",
    position: "relative"
  },
  space: {
    paddingLeft: "24px",
    paddingRight: "24px"
  }
};
export default styles;
