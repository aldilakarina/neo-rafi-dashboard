import React from 'react';
import PropTypes from "prop-types";
import CustomTabs from "components/CustomTabs/CustomTabs.jsx";
import TimelineList from "containers/TimelineList";
import RejectedList from "containers/RejectedList";

export default class Component extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      id: null,
      title: '',
    };
    this._closeModal = this._closeModal.bind(this);
    this._handleDeleteNews = this._handleDeleteNews.bind(this);
  }

  _handleDeleteNews = () => {
    const { actions } = this.props;
    actions.fetchNewsDelete(this.state.id);
    this.setState({ open: false, id:null, title:'' });
  }

  _openModal(id, title) {
    this.setState({ open: true, id, title });
  }

  _closeModal() {
    this.setState({ open: false, id:null, title:'' });
  }

  render() {

    return (
      <CustomTabs
        plainTabs={true}
        headerColor="primary"
        tabs={[
          {
            tabName: "Timeline to Review",
            tabContent: (
              <TimelineList />
            )
          },
          {
            tabName: "Rejected Timeline",
            tabContent: (
              <RejectedList />
            )
          },
        ]}
      />
    );
  }

}

Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
};